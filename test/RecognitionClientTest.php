<?php


namespace XimilarTest\Client;


use PHPUnit\Framework\TestCase;
use Jchook\AssertThrows\AssertThrows;


use Ximilar\Client\Exception\XimilarClientAuthorizationException;
use Ximilar\Client\Exception\XimilarClientException;
use Ximilar\Client\Exception\XimilarClientInvalidDataException;
use Ximilar\Client\Recognition\Model\RecognitionTask;
use Ximilar\Client\Recognition\RecognitionClient;
use Ximilar\Client\Response\ListResponse;
use XimilarTest\Client\BaseTestCase;

class RecognitionClientTest extends BaseTestCase
{
    use AssertThrows; // <--- adds the assertThrows method

    public function testWrongToken() {
        $this->expectException(XimilarClientAuthorizationException::class);
        $client = new RecognitionClient("INVALIDTOKEN");
        $result = $client->listTasks();
    }

    public function testCreateTaskInvalidData() {
        try {
            $client = new RecognitionClient($this->getToken());
        } catch (XimilarClientAuthorizationException $e) {
        }

        $this->assertThrows(XimilarClientInvalidDataException::class, function() use ($client) {
            $client->createTask();
        });
    }

    public function testLabels() {
        $client = new RecognitionClient($this->getToken());
        $label = null;

        // Create some Tasks
        $count = 22;
        for ($i = 1; $i <= $count; $i++) {
            $label = $client->createLabel([
                "name" => "Label $i",
                "description" => "Description $i"
            ]);
            $this->assertEquals("Label $i", $label->name);
            $this->assertEquals("Description $i", $label->description);
        }

        // Work with the last task
        // Check dirty flag
        $this->assertEquals(false, $label->isDirty());
        $label->name = "Other name";
        $this->assertEquals(true, $label->isDirty());

        // Reload without saving
        $label->reload();
        $this->assertEquals("Label $count", $label->name);

        // Change again and save
        $label->name = "Other name";
        $label->save();
        $label->reload();
        $this->assertEquals("Other name", $label->name);

        // Check getter
        $this->assertEquals($label->id, $client->getLabel($label->id)->id);

        // Check immutable fields
        $this->assertThrows(XimilarClientException::class, function() use ($label) {
            $label->id = "Something";
        });

        // List and keep all labels
        $labels = [];
        $response = $client->listLabels();
        do {
            $labels = array_merge($labels, $response->getResults());
            $response = $response->next();
        } while($response);

        // Delete all
        foreach ($labels as $label) {
            $label->delete();
        }

        // Check the list is empty now.
        $response = $client->listLabels();
        $this->assertEquals(0, $response->getCount());
    }

    public function testTasks()
    {
        $client = new RecognitionClient($this->getToken());
        $task = null;

        // Create some Tasks
        $count = 22;
        for ($i = 1; $i <= $count; $i++) {
            $task = $client->createTask([
                "name" => "Task $i",
                "description" => "Description $i"
            ]);
            $this->assertEquals("Task $i", $task->name);
            $this->assertEquals("Description $i", $task->description);
        }

        // Work with the last task
        // Check dirty flag
        $this->assertEquals(false, $task->isDirty());
        $task->name = "Other name";
        $this->assertEquals(true, $task->isDirty());

        // Reload without saving
        $task->reload();
        $this->assertEquals("Task $count", $task->name);

        // Change again and save
        $task->name = "Other name";
        $task->save();
        $task->reload();
        $this->assertEquals("Other name", $task->name);

        // Check getter
        $this->assertEquals($task->id, $client->getTask($task->id)->id);

        // Check immutable fields
        $this->assertThrows(XimilarClientException::class, function() use ($task) {
            $task->id = "Something";
        });

        // List and keep all tasks
        $tasks = [];
        $response = $client->listTasks();
        do {
            $tasks = array_merge($tasks, $response->getResults());
            $response = $response->next();
        } while($response);

        // Delete all
        foreach ($tasks as $task) {
            $task->delete();
        }

        // Check the list is empty now.
        $response = $client->listTasks();
        $this->assertEquals(0, $response->getCount());
    }
}