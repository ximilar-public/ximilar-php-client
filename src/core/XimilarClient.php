<?php


namespace Ximilar\Client;

use Exception;
use GuzzleHttp\Command\Exception\CommandClientException;
use GuzzleHttp\Command\Exception\CommandException;
use GuzzleHttp\Command\ResultInterface;
use Ximilar\Client\Exception\XimilarClientAuthorizationException;
use Ximilar\Client\Exception\XimilarClientException;
use Ximilar\Client\Exception\XimilarClientInvalidDataException;
use Ximilar\Client\Request\Request;
use Ximilar\Client\Response\ListResponse;

/**
 * Class XimilarClient
 *
 * @package Ximilar\Client
 *
 * Base class for Ximilar rest clients
 */
abstract class XimilarClient
{

    protected string $token;

    protected RestClient $client;

    const SERVICE_SLUG = "";
    const SERVICE_NAME = "";

    /**
     * XimilarClient constructor.
     * @param string $token
     * @throws XimilarClientAuthorizationException
     */
    public function __construct(string $token)
    {
        $this->token = $token;

        $this->initClient();
        $this->initResource();
    }


    /**
     * Initializes rest client
     */
    private function initClient()
    {
        $this->client = RestClient::create(
            __DIR__ . '/service.json',
            [
            'headers' => $this->getHeaders()
             ]
        );
    }

    /**
     * Initializes resource of the service
     * @throws XimilarClientAuthorizationException
     * @throws Exception
     */
    private function initResource()
    {
        if (empty($this::SERVICE_SLUG)) {
            throw new Exception("SERVICE_SLUG must be set");
        }
        try {
            $this->client->ResourceCreate(["service" => $this::SERVICE_SLUG]);
        } catch (CommandClientException $e) {
            throw new XimilarClientAuthorizationException("Access to " . $this::SERVICE_NAME . " is denied.");
        }
    }

    /**
     * Sends a list request
     * @param Request $request
     * @return ListResponse
     * @throws XimilarClientException
     */
    public function list(Request $request): ListResponse
    {
        $response = $this->execute($request->getCommand(), $request->getData());
        return new ListResponse($this, $request, $response);
    }

    /**
     * @param string $command
     * @param array $params
     * @return ResultInterface|mixed
     * @throws XimilarClientException
     */
    public function execute(string $command, array $params = []): array
    {
        $gCommand = $this->client->getCommand($command, prepareParams($params));
        try {
            return $this->client->execute($gCommand)->toArray();
        } catch (CommandClientException $e) {
            // Exception from the API
            $this->throwRequestException($e);
        } catch (CommandException $e) {
            // Exception in the requested command
            throw new XimilarClientInvalidDataException($e->getMessage());
        }
    }

    /**
     * @param CommandClientException $e
     * @throws XimilarClientException
     */
    private function throwRequestException(CommandClientException $e)
    {
        $this->throwException($e->getResponse()->getStatusCode(), $e->getResponse()->getBody());
    }

    /**
     * @param int $code
     * @param string $body
     * @throws XimilarClientException
     */
    protected function throwException(int $code, string $body)
    {
        if (in_array($code, [400])) {
            throw new XimilarClientInvalidDataException($body, $code);
        } elseif (in_array($code, [401, 402, 403])) {
            throw new XimilarClientAuthorizationException($body, $code);
        } else {
            throw new XimilarClientException($body, $code);
        }
    }

    /**
     * Returns default headers
     * @return array
     */
    protected function getHeaders(): array
    {
        return [
            "Authorization" => "Token $this->token",
            "User-Agent" => "PHP Client"
        ];
    }

    protected function prepareParams(array $params)
    {
        return $params;
    }
}
