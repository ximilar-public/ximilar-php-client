<?php


namespace Ximilar\Client\Exception;

/**
 * Class XimilarClientException
 * Ximilar Client related exception
 * @package Ximilar\Client
 */
class XimilarClientException extends XimilarException
{

}
