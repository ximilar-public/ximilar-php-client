<?php


namespace Ximilar\Client\Exception;

/**
 * Class XimilarClientAuthorizationException
 * Authorization exception
 * @package Ximilar\Client
 */
class XimilarClientAuthorizationException extends XimilarClientException
{

}
