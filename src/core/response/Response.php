<?php


namespace Ximilar\Client\Response;

use Ximilar\Client\Request\Request;
use Ximilar\Client\XimilarClient;

abstract class Response
{
    protected XimilarClient $client;
    protected Request $request;
    protected array $rawResponse;

    public function __construct(XimilarClient $client, Request $request, array $rawResponse)
    {
        $this->client = $client;
        $this->request = $request;
        $this->rawResponse = $rawResponse;
    }
}
