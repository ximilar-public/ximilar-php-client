<?php


namespace Ximilar\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Command\Guzzle\Description;
use GuzzleHttp\Command\Guzzle\GuzzleClient;

/**
 * @method ResourceCreate(array $array)
 * @method RecognitionTaskRetrieve(array $array)
 */
class RestClient extends GuzzleClient
{

    public static function create(string $service_file, array $config)
    {
        // Load the service description file.
        $service_description = new Description(
            (array)json_decode(file_get_contents($service_file), true)
        );

        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];

        if (key_exists('headers', $config)) {
            $headers = $config['headers'] + $headers;
        }

        // Creates the client and sets the default request headers.
        $client = new Client(
            [
            'headers' => $headers,
                'debug' => false
            ]
        );

        return new static($client, $service_description, null, null, null, $config);
    }
}
