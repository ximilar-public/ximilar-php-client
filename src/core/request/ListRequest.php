<?php


namespace Ximilar\Client\Request;

/**
 * Class ListRequest
 * Request object for lists
 * @package Ximilar\Client\Request
 *
 */
class ListRequest extends Request
{

    public function __construct(string $command, int $page = 1)
    {
        parent::__construct($command);
        $this->setData([
           "page" => $page
        ]);
    }
}
