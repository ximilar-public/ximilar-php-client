<?php


namespace Ximilar\Client\Request;

use Ximilar\Client\Exception\XimilarClientInvalidRequestException;

abstract class Request
{

    private string $command;

    protected array $data;
    protected string $modelClass;

    public function __construct(string $command)
    {
        $this->command = $command;
    }

    /**
     * Clones the request with given params to overwrite
     * @param array $params
     * @return Request
     */
    public function clone(array $params = [])
    {
        $request = clone $this;
        $request->applyData($params);
        return $request;
    }

    /**
     * @param string $command
     * @throws XimilarClientInvalidRequestException
     */
    public function checkCommand(string $command)
    {
        if ($this->command !== $command) {
            throw new XimilarClientInvalidRequestException("This is not a $command request");
        }
    }

    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return $this->modelClass;
    }

    /**
     * @param string $modelClass
     */
    public function setModelClass(string $modelClass): void
    {
        $this->modelClass = $modelClass;
    }

    /**
     * @return string
     */
    public function getCommand(): string
    {
        return $this->command;
    }

    /**
     * Returns list of data associated with this request
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Rewrites request parameters
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * Adds / replaces data items by given array
     * @param array $data
     */
    public function applyData(array $data)
    {
        $this->data = $data + $this->data;
    }
}
