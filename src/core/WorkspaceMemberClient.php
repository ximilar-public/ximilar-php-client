<?php


namespace Ximilar\Client;

use Ximilar\Client\Request\ListRequest;

class WorkspaceMemberClient extends XimilarClient
{
    protected string $workspace;

    public function __construct(string $token, string $workspace = null)
    {
        parent::__construct($token);

        if ($workspace) {
            $this->workspace = $workspace;
        }
    }

    /**
     * Simple function to return an array with workspace => WORKSPACE_id
     * @return array
     */
    private function getWorkspaceArray(): array
    {
        return ["workspace" => $this->workspace];
    }

    /**
     * Creates and returns new list request
     * @param mixed ...$args
     * @return ListRequest
     */
    protected function createListRequest(...$args): ListRequest
    {
        $request = new ListRequest(...$args);
        if (!empty($this->workspace)) {
            $request->applyData($this->getWorkspaceArray());
        }
        return $request;
    }

    protected function prepareParams(array $params): array
    {
        return $this->getWorkspaceArray() + parent::prepareParams($params);
    }
}
