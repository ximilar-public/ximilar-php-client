<?php

namespace Ximilar\Client\Recognition\Model;


use Ximilar\Client\Model\XimilarModel;
use const Ximilar\Client\Recognition\CMD_RECOGNITION_TASK_DELETE;
use const Ximilar\Client\Recognition\CMD_RECOGNITION_TASK_PATCH;
use const Ximilar\Client\Recognition\CMD_RECOGNITION_TASK_RETRIEVE;

/**
 * @property string|null description
 * @property string name
 */
class RecognitionTask extends XimilarModel
{
    const CMD_DELETE = CMD_RECOGNITION_TASK_DELETE;
    const CMD_RETRIEVE = CMD_RECOGNITION_TASK_RETRIEVE;
    const CMD_PATCH = CMD_RECOGNITION_TASK_PATCH;
}
