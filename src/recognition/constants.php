<?php

namespace Ximilar\Client\Recognition;

// Recognition Task
const CMD_RECOGNITION_TASKS_LIST = "RecognitionTasksList";
const CMD_RECOGNITION_TASK_DELETE = "RecognitionTaskDelete";
const CMD_RECOGNITION_TASK_RETRIEVE = "RecognitionTaskRetrieve";
const CMD_RECOGNITION_TASK_CREATE = "RecognitionTaskCreate";
const CMD_RECOGNITION_TASK_PATCH = "RecognitionTaskPatch";

// Recognition Label
const CMD_RECOGNITION_LABEL_LIST = "RecognitionLabelList";
const CMD_RECOGNITION_LABEL_DELETE = "RecognitionLabelDelete";
const CMD_RECOGNITION_LABEL_RETRIEVE = "RecognitionLabelRetrieve";
const CMD_RECOGNITION_LABEL_CREATE = "RecognitionLabelCreate";
const CMD_RECOGNITION_LABEL_PATCH = "RecognitionLabelPatch";