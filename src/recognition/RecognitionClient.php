<?php


namespace Ximilar\Client\Recognition;

use GuzzleHttp\Command\ResultInterface;
use Tightenco\Overload\Overloadable;

use Ximilar\Client\WorkspaceMemberClient;
use Ximilar\Client\Recognition\Model\RecognitionLabel;
use Ximilar\Client\Recognition\Model\RecognitionTask;
use Ximilar\Client\Request\ListRequest;
use Ximilar\Client\Response\ListResponse;
use Ximilar\Client\Exception\XimilarClientException;


class RecognitionClient extends WorkspaceMemberClient
{
    // https://gist.github.com/adamwathan/120f5acb69ba84e3fa911437242796c3
    use Overloadable;

    const SERVICE_SLUG = "custom-recognition";
    const SERVICE_NAME = "Custom Recognition";

    // -------------------------------------------------------------------
    // TASKS
    // -------------------------------------------------------------------

    /**
     * @param array $args
     * @return ResultInterface|mixed
     */
    public function listTasks(...$args): ListResponse
    {
        return $this->overload($args, [
            function (ListRequest $request) {
                $request->checkCommand(CMD_RECOGNITION_TASKS_LIST);
                return $this->list($request);
            },
            function (int $page) {
                $request = $this->createListRequest(CMD_RECOGNITION_TASKS_LIST, $page);
                $request->setModelClass(RecognitionTask::class);
                return $this->listTasks($request);
            },
            function () {
                return $this->listTasks(1);
            }
        ]);
    }

    /**
     * @param string $id
     * @return RecognitionTask|null
     * @throws XimilarClientException
     */
    public function getTask(string $id): RecognitionTask
    {
        return new RecognitionTask($this->execute(CMD_RECOGNITION_TASK_RETRIEVE, ["id" => $id]), $this);
    }

    /**
     * @param mixed ...$args
     * @return RecognitionTask
     * @throws XimilarClientException
     */
    public function createTask(...$args): RecognitionTask
    {
        return new RecognitionTask($this->execute(CMD_RECOGNITION_TASK_CREATE, $args), $this);
    }

    // -------------------------------------------------------------------
    // LABELS
    // -------------------------------------------------------------------

    /**
     * @param array $args
     * @return ResultInterface|mixed
     */
    public function listLabels(...$args): ListResponse
    {
        return $this->overload($args, [
            function (ListRequest $request) {
                $request->checkCommand(CMD_RECOGNITION_LABEL_LIST);
                return $this->list($request);
            },
            function (int $page) {
                $request = $this->createListRequest(CMD_RECOGNITION_LABEL_LIST, $page);
                $request->setModelClass(RecognitionLabel::class);
                return $this->listLabels($request);
            },
            function () {
                return $this->listLabels(1);
            }
        ]);
    }

    /**
     * @param string $id
     * @return RecognitionLabel|null
     * @throws XimilarClientException
     */
    public function getLabel(string $id): RecognitionLabel
    {
        return new RecognitionLabel($this->execute(CMD_RECOGNITION_LABEL_RETRIEVE, ["id" => $id]), $this);
    }

    /**
     * @param mixed ...$args
     * @return RecognitionLabel
     * @throws XimilarClientException
     */
    public function createLabel(...$args): RecognitionLabel
    {
        return new RecognitionLabel($this->execute(CMD_RECOGNITION_LABEL_CREATE, $args), $this);
    }

}
